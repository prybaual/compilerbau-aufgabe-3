#include "minako.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int currentToken, nextToken;
void program(void);
void functiondefinition(void);
void functioncall(void);
void statementlist(void);
void block(void);
void statement(void);
void ifstatement(void);
void returnstatement(void);
void print(void);
void type(void);
void statassignement(void);
void assignement(void);
void expr(void);
void simpexpr(void);
void term(void);
void factor(void);
void isTokenAndEat(int token);
void eat(void);
int toInt(char c);
int isNextToken(int token);
int isToken(int token);

void program(void){
	/*(functiondefinition)+ or EOF*/
	while(currentToken != EOF)
		functiondefinition();
		
	exit(0);
}

void functiondefinition(void){
	type();
	isTokenAndEat(ID);
	isTokenAndEat(toInt('('));
	isTokenAndEat(toInt(')'));
	isTokenAndEat(toInt('{'));
	statementlist();
	isTokenAndEat(toInt('}'));
	return;
}

void functioncall(void){
	isTokenAndEat(ID);
	isTokenAndEat(toInt('('));
	isTokenAndEat(toInt(')'));
	return;
}

void statementlist(void){
	/* '}' must follow after statementlist so we don't need to rewrite (block)* with recursive function*/
	while(!(isToken(toInt('}'))))
		block();
	return;
}

void block(void){
	if(isToken(toInt('{'))){
		eat();
		statementlist();
		isTokenAndEat(toInt('}'));
		return;
	}
	statement();
	return;
}

void statement(void){
	if(isToken(KW_IF)){
		ifstatement();
		return;		/*only rule without following ';'*/
	}
	else if(isToken(KW_RETURN)){
		returnstatement();
	}
	else if(isToken(KW_PRINTF)){
		print();
	}
	else if(isToken(ID)){	/*need to seperate if functioncall or statassignement follows (both start with ID)*/
		if(isNextToken(toInt('=')))	/* ID with following '=' means we need to apply rule statassignement */
			statassignement();
		else if(isNextToken(toInt('(')))	/* ID with following '(' means we need to apply rule functioncall */
			functioncall();
	}
	isTokenAndEat(toInt(';'));
	return;
}

void ifstatement(void){
	isTokenAndEat(KW_IF);
	isTokenAndEat(toInt('('));
	assignement();
	isTokenAndEat(toInt(')'));
	block();
	return;
}

void returnstatement(void){
	isTokenAndEat(KW_RETURN);
	if(isToken(toInt(';')))	/*optional rule assignement -> if token = FOLLOW(returnstatement) = ';' don't apply assignement*/ 
		return; 	/*no assignement*/
	assignement();
	return;
}

void print(void){
	isTokenAndEat(KW_PRINTF);
	isTokenAndEat(toInt('('));
	assignement();
	isTokenAndEat(toInt(')'));
	return;
}

void type(void){
	if(isToken(KW_BOOLEAN) || isToken(KW_FLOAT) || isToken(KW_INT) || isToken(KW_VOID)){
		eat();
		return;
	}
	else{
		fprintf(stderr, "ERROR: Syntaxfehler in Zeile %d\n", yylineno);
		exit(-1);
	}
}

void statassignement(void){
	isTokenAndEat(ID);
	isTokenAndEat(toInt('='));
	assignement();
	return;
}

void assignement(void){
	/*alternative*/
	if(isToken(ID) && isNextToken(toInt('='))){	/*first two tokens = ID and '=' -> apply rule assignement */
		eat();
		eat();
		assignement();
	}
	else expr();	
	return;
}

void expr(void){
	simpexpr();
	/* optional sign with following simpexpr so check if Token is one of th signs */
	if(isToken(EQ) || isToken(NEQ) || isToken(LEQ) || isToken(GEQ) || isToken(LSS)
			|| isToken(GRT)){
		eat();
		simpexpr();
	}
	return;
}

void simpexpr(void){
	if(isToken(toInt('='))) eat();	/*optional '=' thus no use of isTokenAndEat */
	term();
	/* elements which could follow term and elements in kleene star are not the same so we can check if an 
	element from kleene star follows and apply appropriate rule, else no elements from kleene star -> return */
	while(isToken(toInt('+')) || isToken(toInt('-')) || isToken(OR)){
		eat();
		term();
	}
	return;
}

void term(void){
	factor();
	/* kleene star here has identic properties like the one in simpexpr */
	while(isToken(toInt('*')) || isToken(toInt('/')) || isToken(AND)){
		eat();
		factor();
	}
	return;
}

void factor(void){
	if(isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN)){
		eat();
		return;
	}	
	else if(isToken(toInt('('))){	/*'(' means we apply rule '(' assignement ')'*/
		eat();
		assignement();
		isTokenAndEat(toInt(')'));
	}
	else if(isToken(ID)){	/* if Token is ID we don't know if we shoueld aplly rule functioncall or rule <ID> */
		if(isNextToken(toInt('(')))	/*following '(' implifies we need to aplly rule functioncall */
			functioncall();
		else eat();

		return;
	}
	else{
		fprintf(stderr, "ERROR: Syntaxfehler in Zeile %d\n", yylineno);
		exit(-1);
	}
	return;
}

/*function to return int-value of an char to keep the code more readable*/
int toInt(char c){
	return (int)c;
}

/* function checks if nextToken and parameter int token are identic*/
int isNextToken(int token){
	if(token == nextToken)
		return 1;

	return 0;
}

/*checks if currentToken and parameter int token are identic*/
int isToken(int token){
	if(token == currentToken){
		return 1;
	}
	return 0;
}

/*'eats' currentToken, reads new Token*/
void eat(void){
	currentToken = nextToken;
	nextToken = yylex();
	return;
}

/*if int token = currentToken eat it*/
void isTokenAndEat(int token){
	if(isToken(token)){
		eat();
		return;
	}
	else{
		fprintf(stderr, "ERROR: Syntaxfehler in Zeile %d\n", yylineno);
		exit(-1);
	}
}

/*main function, read from stdin or fileand start with state program*/
int main(int argc, char** argv){

    if (argc != 2)
		yyin = stdin;
	else
	{
		yyin = fopen(argv[1], "r");
		if (yyin == 0)
		{
			fprintf(stderr, "Fehler: Konnte Datei %s nicht zum lesen oeffnen.\n", argv[1]);
			exit(-1);
		}
	}

	if((currentToken = yylex()) == EOF){
		fprintf(stderr,"Empty input");
	}
	nextToken = yylex();
	program();
}